var express = require('express')
var app = express()
app.set('views', './src/template')
app.set('view engine', 'pug')

app.use('/img', express.static('build/img'))
app.use('/css', express.static('build/css'))
app.use('/fonts', express.static('build/fonts'))
app.use('/js', express.static('build/js'))

app.get('/*', function(req, res) {
  res.render(req.path.slice(1))
})

app.listen(9001, function() {
  // console.log('Example app listening on port 3000!')
})
