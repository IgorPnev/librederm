export default function search() {
  const searchButton = document.querySelector(".buttonSearch")
  const searchInputDiv = document.querySelector(".headerSearchInput")
  const searchInput = searchInputDiv.querySelector("input")
  const searchPanel = document.querySelector(".headerSearchPanel")

  searchButton.addEventListener("click", () => {
    searchInputDiv.classList.toggle("headerSearchInputIsShow")
  })

  searchInput.addEventListener("input", () => {
    searchPanel.classList.add("headerSearchPanelIsShow")
  })
}
