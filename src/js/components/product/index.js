import Swiper from 'swiper/dist/js/swiper.js'

export default function productSlider() {
  // let swiperProduct = ''
  // let swiperThumbProduct = ''
  const sliderProduct = document.querySelector('.productImageSlider')
  // const sliderThumbProduct = document.querySelector('.productImageSliderThumbs')

  if (document.body.contains(sliderProduct)) {
    const swiperProduct = new Swiper('.productImageSlider', {
      slidesPerView: 1,
      spaceBetween: 0,
      effect: 'fade',
      loop: true,
      fade: {
        crossFade: true,
      },
      loopedSlides: 4,
    })

    const swiperThumbProduct = new Swiper('.productImageSliderThumbs', {
      slidesPerView: 5,
      spaceBetween: 0,
      loop: true,
      slideToClickedSlide: true,
      nextButton: '.productImageSliderThumbRight',
      prevButton: '.productImageSliderThumbLeft',
      loopedSlides: 4,
      breakpoints: {
        1715: {
          slidesPerView: 4,
        },
        1300: {
          slidesPerView: 3,
        },
        1040: {
          slidesPerView: 5,
        },
        768: {
          slidesPerView: 3,
        },
        600: {
          slidesPerView: 2,
        },
      },
    })

    swiperProduct.params.control = swiperThumbProduct
    swiperThumbProduct.params.control = swiperProduct

    const swiperTabProduct = new Swiper('.productTabControls', {
      slidesPerView: 'auto',
      spaceBetween: 0,
      loop: false,
      slideToClickedSlide: true,
      preventClicks: false,
      preventClicksPropagation: false,
      onClick: swiper => {
        console.log(swiper)
      },
    })
  }
}
