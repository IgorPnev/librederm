export default function passwordShow() {
  const inputs = Array.from(document.querySelectorAll('.uiTextInputGroupPassword input'))
  const button = document.querySelector('.showPassword')
  if (button) {
    button.addEventListener('click', () => {
      button.classList.toggle('showPasswordActive')
      inputs.map(input => {
        if (input.type === 'password') {
          input.type = 'text'
        } else {
          input.type = 'password'
        }
      })
    })

    const changeButton = document.querySelector('.cabinetProfileChangePasswordButton')
    const containerPassword = document.querySelector('.cabinetProfileFormPassword')
    changeButton.addEventListener('click', () => {
      containerPassword.classList.toggle('cabinetProfileFormPasswordIsNew')
    })
  }
}
