import Swiper from 'swiper/dist/js/swiper.js'

export default function colorSlider() {
  const slider = document.querySelector('.cardProductColors')
  if (slider) {
    let swiper = new Swiper('.cardProductColors', {
      slidesPerView: '5',
      spaceBetween: 15,
      nextButton: '.cardProductColorsNext',
      loop: false,
    })
  }
}
