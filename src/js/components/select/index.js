import Choices from 'choices.js'

export default function select() {
  const elements = Array.from(document.querySelectorAll('.choices'))
  if (elements.length > 0) {
    const choices = new Choices('.choices', {
      search: false,
      shouldSort: false,
      shouldSortItems: false,
    })
    choices.init()
  }
}
