import houdini from 'houdini'

export default function houdiniInit() {
  houdini.init()
}
