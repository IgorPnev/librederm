export default function filter() {
  const filters = Array.from(document.querySelectorAll('.filterItem'))
  const filtersItem = document.querySelectorAll('.filterItem')

  filters.map((item, index) => {
    item.addEventListener('click', () => {
      if (!item.classList.contains('filterItemActive')) closeAllFilter()
      item.classList.toggle('filterItemActive')
      const dropdown = item.querySelector('.filterItemDropdown')
      const top = item.getBoundingClientRect().top + pageYOffset
      const topItem = document.querySelector('.filterItems').getBoundingClientRect().top + pageYOffset
      const offsetTop = top - topItem + 45
      dropdown.style.top = offsetTop + 'px'
    })
  })
  const closeAllFilter = () => {
    filters.map(item => {
      item.classList.remove('filterItemActive')
    })
  }
}
