import Swiper from 'swiper/dist/js/swiper.js'

export default function mainSlider() {
  const slider = document.querySelector('.mainSlider')
  if (slider) {
    let swiper = new Swiper('.mainSlider', {
      slidesPerView: '1',
      spaceBetween: 0,
      prevButton: '.mainSliderPrev',
      nextButton: '.mainSliderNext',
      pagination: '.mainSliderPagginator',
      paginationClickable: true,
      loop: true,
    })

    slider.addEventListener('mouseenter', () => {
      swiper.stopAutoplay()
    })
    slider.addEventListener('mouseleave', () => {
      swiper.startAutoplay()
    })
  }
}
