import Swiper from 'swiper/dist/js/swiper.js'

export default function pageSlider () {
  const slider = document.querySelector('.pageSlider')
  if (slider) {
    let swiper = new Swiper('.pageSlider', {
      slidesPerView: 'auto',
      spaceBetween: 0,
      loop: true,
      nextButton: '.pageSliderNext',
      prevButton: '.pageSliderPrev'
    }
     )
  }
}
