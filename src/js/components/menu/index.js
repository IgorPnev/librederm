export default function largeMenu() {
  const menuButton = document.querySelector('.headerMenuItemAll svg')
  const menuItem = document.querySelector('.headerMenuItemAll')
  const menu = document.querySelector('.largeMenu')

  menuButton.addEventListener('click', () => {
    menu.classList.toggle('largeMenuIsShow')
    menuItem.classList.toggle('headerMenuItemAllIsOpen')
  })

  const mobileMenuButton = Array.from(document.querySelectorAll('.mobileHeaderMenu'))
  const mobileMenuSearch = Array.from(document.querySelectorAll('.mobileHeaderSearch'))
  const mobileMenu = document.querySelector('.mobileMenuContainer')

  mobileMenuButton.map(el => {
    el.addEventListener('click', () => {
      showOverlay()
      const modalClose = mobileMenu.querySelector('.mobileMenuClose')
      const overlay = document.querySelector('.overlay')
      mobileMenu.classList.add('mobileMenuIsShow')
      modalClose.addEventListener('click', () => {
        mobileMenu.classList.remove('mobileMenuIsShow')
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
      overlay.addEventListener('click', () => {
        mobileMenu.classList.remove('mobileMenuIsShow')
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
    })
  })

  mobileMenuSearch.map(el => {
    el.addEventListener('click', () => {
      showOverlay()
      const modalClose = mobileMenu.querySelector('.mobileMenuClose')
      const overlay = document.querySelector('.overlay')
      mobileMenu.classList.add('mobileMenuIsShow')
      modalClose.addEventListener('click', () => {
        mobileMenu.classList.remove('mobileMenuIsShow')
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
      overlay.addEventListener('click', () => {
        mobileMenu.classList.remove('mobileMenuIsShow')
        setTimeout(() => {
          hideOverlay()
        }, 300)
      })
    })
  })
}

const showOverlay = () => {
  const overlay = document.querySelector('.overlay')
  const body = document.querySelector('body')
  overlay.classList.add('overlayShow')
  body.classList.add('modalBodyShow')
}

const hideOverlay = () => {
  const overlay = document.querySelector('.overlay')
  const body = document.querySelector('body')
  overlay.classList.remove('overlayShow')
  body.classList.remove('modalBodyShow')
}

function isHover(e) {
  return e.parentElement.querySelector(':hover') === e
}
