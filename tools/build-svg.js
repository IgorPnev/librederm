import { dest, src } from 'gulp'

import cache from 'gulp-cached'
import plumber from 'gulp-plumber'
import remember from 'gulp-remember'
import rename from 'gulp-rename'
import svgmin from 'gulp-svgmin'
import svgstore from 'gulp-svgstore'

export const buildSvg = () =>
  src('./src/svg/**/*.svg')
    .pipe(plumber())
    .pipe(
      svgmin(function() {
        return {
          plugins: [
            { collapseGroups: true },
            { removeStyleElement: true },
            { minifyStyles: true },
            {
              convertStyleToAttrs: true,
            },
          ],
        }
      })
    )
    .pipe(
      svgstore({
        inlineSvg: true,
      })
    )
    .pipe(rename('sprite.svg'))
    .pipe(dest('./build/img/'))
